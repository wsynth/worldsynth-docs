# Gradient

![header_gradient](images/header_gradient.png)

The gradient module generates a heightmap in the linear gradient in a uniform direction. The direction, scale and shape of the gradient can be adjusted.

## Inputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Scale` <label class="sd-text-secondary">Scalar</label>

    Scalar input determining the scale of the ramp.

    ```{admonition} Optional
    :class: tip

    This input is optional.
    
    If input is provided, the provided value is used instead of the the `Scale` parameter.
    ```

    ---

- {bdg-dark}`Direction` <label class="sd-text-secondary">Scalar or heightmap</label>

    A scalar or heightmap input determining the direction of the gradient.
    
    Using the heightmap enables the generation of outputs with non-uniform gradient directions.

    ```{admonition} Optional
    :class: tip

    This input is optional.
    
    If input is provided as a scalar, the provided value is used instead of the the `Direction` parameter.
    
    If the input is provided as a heightmap, normalized values from the heightmap are mapped to one full rotation of the gradient direction and applied contextually, replacing the value given by the the `Direction` parameter.
    ```

    ---

- {bdg-dark}`Distortion` <label class="sd-text-secondary">Heightmap or vectormap</label>

    A heightmap or vectormap input controlling distortion of the gradient.

    ```{admonition} Optional
    :class: tip

    This input is optional.

    If input is provided as a heightmap, the provided values are contextually mapped into directions, together with the `Distortion` parameter as the distance, this determines the distortion offset for sampling the gradient.
    
    If the input is provided as a vectormap, the values from the vectormap are multiplied with the `Distortion` parameter and used contextually as the distortion offset for sampling the gradient.
    ```

    ---

- {bdg-dark}`Mask` <label class="sd-text-secondary">Heightmap</label>

    Generator mask

    ```{admonition} Optional
    :class: tip

    This input is optional.
    ```

## Outputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Output` <label class="sd-text-secondary">Heightmap</label>

    Generated gradient heightmap

## Parameters
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Scale`

    The scale/period of the generated gradient.
    
    This is the length of the gradient needed to move from 0 to the normalized height value, or the length needed to continue one tiling cycle.

- {bdg-dark}`Direction`

    The direction of the generated gradient. The value is in an angle of degrees, increasing in the counterclockwise direction.

- {bdg-dark}`Tiling`

    - **NONE**

        The gradient is unclamped and non-repeating.
        
        The gradient continues bellow 0 and beyond the normalized height.

        ![Gradient tiling NONE](images/gradient_tiling_none.png)

    - **CLAMPED**

        The gradient is clamped and non-repeating.
        
        The values are clamped between 0 and the normalized height, not continuing down into the negative values beyond 0, or higher than the normalized height.

        ![Gradient tiling CLAMPED](images/gradient_tiling_clamped.png)

    - **TILING**

        The gradient is clamped and repeating in a discontinuous manner.

        The value of the gradient cycles in a repeating pattern, starting back from 0 when the max value is reached.

        ![Gradient tiling TILING](images/gradient_tiling_tiling.png)

    - **CONTINUOUS**

        The gradient is clamped and repeating in a continuos manner.

        The value of the gradient cycles in a repeating pattern, cycling back down to 0 when the max value is reached, and then repeating the increasing-decreasing pattern onwards.

        ![Gradient tiling CONTINUOUS](images/gradient_tiling_continuous.png)

- {bdg-dark}`Distortion`

    The distortion strength used when sampling the gradient. This requires an input provided to the `Distortion` input to have effect.