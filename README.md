# WorldSynth Docs
The latest docs are automatically published to https://wsynth.gitlab.io/worldsynth-docs/

## Getting started
These docs uses Mudkip, a friendly Sphinx wrapper.

The Mudkip package can be installed with `pip`:
```bash
$ pip install mudkip
```

For further details about, and getting started with Mudkip, please refer to Mudkip's documentation, https://github.com/vberlier/mudkip.

These documentation also used the Furo theme, and the Sphinx Design extension, so after installing Mudkip, also install these:
```bash
$ pip install furo
$ pip install sphinx-design
```

To then build the documentation as you're working on them, you can run the command:
```bash
$ mudkip develop --preset furo
```
This will build the docs and serve them from a local web server that can be accessed on: http://localhost:5500