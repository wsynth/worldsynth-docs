# Vectormap modules

```{toctree}
:hidden:

```

## Generators
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Vectormap constant
:::

:::{grid-item-card} Vectormap component
:::

:::{grid-item-card} Vectormap gradient
:::

::::

## Generators (Heightmap)
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Vectormap absolute
:::

:::{grid-item-card} Vectormap angle
:::

:::{grid-item-card} Vectormap dot product
:::

::::

## Modifiers
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Vectormap normalize
:::

:::{grid-item-card} Vectormap rotate
:::

:::{grid-item-card} Translate
:::

::::

## Combiners
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Vectormap combiner
:::

::::
