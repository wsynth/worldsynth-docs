# Constant

![header_constant](images/header_constant.png)

The constant module generates a heightmap of constant height given by the `Constant` parameter.

## Inputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Height` <label class="sd-text-secondary">Scalar</label>

    Scalar input determining the height

    ```{admonition} Optional
    :class: tip

    This input is optional. If input is provided, the provided value is used instead of the the `Constant` parameter.
    ```

- {bdg-dark}`Mask` <label class="sd-text-secondary">Heightmap</label>

    Generator mask

    ```{admonition} Optional
    :class: tip

    This input is optional.
    ```

## Outputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Output` <label class="sd-text-secondary">Heightmap</label>

    Generated constant heightmap

## Parameters
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Constant`

    The constant height value to used for the output heightmap