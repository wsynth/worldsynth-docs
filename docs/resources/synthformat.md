# Synth save format

The WorldSynth save format, with the extension `.wsynth`, is a JSON based save format for WorldSynth projects. 

## Synth object specification
The synth object is the root node for the save format.
```json
{
    "version" : {
        "worldsynth" : "0.5.0", // The WorldSynth release version the save was created by.
        "saveformat" : 1, // A version identifier for the storage format version used.
    },
    "name" : "ProjectName", // The name of the WorldSynth project.
    "extents" : [
        // A list of extent objects, see the extents object specification.
    ],
    "parameters" : {
        // A collection of synth-wide project specific parameters.
        // Currently only the "normalized heightmap height" goes here.
        "normalizedheightmapheight" : 256.0
    },
    "patches" : [
        // A list of patch objects, see the patch object specification.
        // Currently any project will only contain one patch.
    ]
}
```

## Extent object specification
```json
{
    "id" : 1, // A unique numeric id generated for the extent.
    "name" : "Default extent 1/4k", // The displayed name for the extent.
    "sizeratiolock" : true, // Wether the width/length is locked in a ratio during editing.

    // Position and size of the extent.
    "x" : -128.0,
    "y" : 0.0,
    "z" : -128.0,
    "width" : 256.0,
    "height" : 256.0,
    "length" : 256.0
}
```

## Patch object specification
```json
{
    "name" : "", // A custom name for the patch. Currently not used.
    "modules" : [
        // A list of module objects, see the module object specification.
    ],
    "connectors" : [
        // A list of connection objects, see the connection object specification.
    ]
}
```

## Module object specification
```json
{
    "id" : "moduleheightmapgain0", // A unique id generated for the module.
    "name" : "", // A custom name given to the module, which is displayed in the editor.

    // The position of the module in the editor.
    "x" : 596.9073798400466,
    "y" : -164.0596484280954,

    "bypass" : false, // Wether this module is bypassed.
    "moduleclass" : "net.worldsynth.module.heightmap.ModuleHeightmapGain", // The class name of the module implementation.
    "parameters" : {
        // A collection of module specific parameters.
        // These varies depending on the module.
        "gain" : 1.0,
        "offset" : -8.0,
        "gaincentre" : 0.0
    }
}
```

## Module connector object specification
```json
{
    // Source
    // Where the connector connects from the output of.
    "module1id" : "moduleheightmapgain0", // The id of the source module.
    "module1io" : "Output", // The name of the source module output.

    // Destination
    // Where the connector connects to the input of.
    "module2id" : "moduleheightmapsimpleperlin0", // The id of the destination module.
    "module2io" : "Mask" // The name of the destination module input.
}
```