# Increasing RAM

By default WorldSynth launches with a maximum of 4 GB RAM allocation. This maximum can be increased from the launch configuration file, `launchconfig.json` in the root folder.

By default the contents of `launchconfig.json` looks something like this:

```json
{
    "JRE_WINDOWS":"system-jre",
    "JRE_LINUX":"system-jre",
    "JRE_MAC":"system-jre",
    "JRE_XMS":"-Xms512M",
    "JRE_XMX":"-Xmx4G",
    "DEFAULT_JVM_OPTS": [
        "--add-exports=javafx.base/com.sun.javafx.event=ALL-UNNAMED",
        "--add-exports=javafx.graphics/com.sun.javafx.stage=ALL-UNNAMED",
        "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=ALL-UNNAMED",
        "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED"],
    "MAIN":"net.worldsynth.editor.WorldSynthEditorApp",
    "PERSIST_TERM":true
}

```

To increase the RAM allocation, change the `JRE_XMX` value, for example:

**8 GB**
```json
"JRE_XMX":"-Xmx8G"
```

**12 GB**
```json
"JRE_XMX":"-Xmx12G"
```

**24 GB**
```json
"JRE_XMX":"-Xmx24G"
```