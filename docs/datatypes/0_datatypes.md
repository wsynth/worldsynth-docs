# Datatypes

```{toctree}
:hidden:

scalar
heightmap
colormap
materialmap
biomemap
featuremap
vectormap
valuespace
blockspace
biomespace
featurespace
vectorspace
objects
```