# WorldSynth

WorldSynth is an open source, graphically customizable terrain generator with support for generating voxel terrains. It's initially made with Minecraft in mind but might be usable for many other applications to.

WorldSynth uses a graph based workflow and generators can be edited using the editor.

```{admonition} 🚧 Documentation under construction 🚧
:class: warning

These docs are heavily in-progress, many parts are still missing, and they may change as we improve them.
```

# WorldSynth tutorials:
[Tutorial 01: Making your fist terrain with WorldSynth](tutorials/tutorial01/tutorial01.md)


```{toctree}
:caption: WorldSynth
:hidden:

synth
datatypes/0_datatypes
addons
```

```{toctree}
:caption: Editor
:hidden:

editor/patcher
```

```{toctree}
:caption: Modules
:hidden:

modules/biomemap/0_modules
modules/biomespace/0_modules
modules/blockspace/0_modules
modules/colormap/0_modules
modules/featuremap/0_modules
modules/featurespace/0_modules
modules/heightmap/0_modules
modules/materialmap/0_modules
modules/objects/0_modules
modules/scalar/0_modules
modules/valuespace/0_modules
modules/vectormap/0_modules

```

```{toctree}
:caption: Tutorials
:hidden:

tutorials/tutorial01/tutorial01
```

```{toctree}
:caption: Resources
:hidden:

resources/increase_ram
resources/materialsformat
resources/biomesformat
resources/synthformat
```

```{toctree}
:caption: Developer resources
:hidden:


```

```{toctree}
:caption: Links
:hidden:

WebPage <https://www.worldsynth.net/>
GitLab <https://gitlab.com/wsynth>
```