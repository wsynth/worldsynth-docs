# Blockspace modules

```{toctree}
:hidden:

```

## Generators
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Blockspace from heightmap
:::

:::{grid-item-card} Blockspace from valuespace
:::

:::{grid-item-card} Perlin worm
:::

:::{grid-item-card} Strata from valuespace
:::

::::

## Modifiers
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Affine transform
:::

:::{grid-item-card} Cellularize
:::

:::{grid-item-card} Distort
:::

:::{grid-item-card} Filter
:::

:::{grid-item-card} Blockspace height clamp
:::

:::{grid-item-card} Isolate surface
:::

:::{grid-item-card} Layered cellularize
:::

:::{grid-item-card} Surface cover
:::

:::{grid-item-card} Translate
:::

::::

## Combiners
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Blockspace combiner
:::

::::
