# Fractal perlin

![header_fractal_perlin](images/header_fractal_perlin.png)

The fractal perlin module generates a heightmap from multiple octaves of a perlin noise generator.

The octaves are multiple iterations of perlin noise, each consecutive iteration half the scale of the last, and with a consecutively reduced amplitude for each iteration determined acording to the `Persistence` parameter.

## Inputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Scale` <label class="sd-text-secondary">Scalar</label>

    Scalar input determining the scale of the noise

    ```{admonition} Optional
    :class: tip

    This input is optional. If input is provided, the provided value is used instead of the the `Scale` parameter.
    ```

    ---

- {bdg-dark}`Amplitude` <label class="sd-text-secondary">Scalar or heightmap</label>

    A scalar or heightmap input determining the amplitude of the noise.

    ```{admonition} Optional
    :class: tip

    This input is optional.
    
    If input is provided as a scalar, the provided value is used instead of the the `Scale` parameter.
    
    If the input is provided as a heightmap, normalized values from the heightmap are used contextually to multiplicatively modulate the value given by the the `Scale` parameter.
    ```

    ---


- {bdg-dark}`Persistence` <label class="sd-text-secondary">Scalar or heightmap</label>

    A scalar or heightmap input determining the persistence of the noise.

    ```{admonition} Optional
    :class: tip

    This input is optional.
    
    If input is provided as a scalar, the provided value is used instead of the the `Persistence` parameter.
    
    If the input is provided as a heightmap, normalized values from the heightmap are used contextually to multiplicatively modulate the value given by the the `Persistence` parameter.
    ```

    ---

- {bdg-dark}`Offset` <label class="sd-text-secondary">Scalar or heightmap</label>

    A scalar or heightmap input determining the offset of the noise.

    ```{admonition} Optional
    :class: tip

    This input is optional.
    
    If input is provided as a scalar, the provided value is used instead of the the `Offset` parameter.
    
    If the input is provided as a heightmap, normalized values from the heightmap are used contextually to additively modulate the value given by the the `Offset` parameter. Setting the `Offset` parameter to 0 effectively makes this input a non-modulating control.
    ```

    ---

- {bdg-dark}`Distortion` <label class="sd-text-secondary">Heightmap or vectormap</label>

    A heightmap or vectormap input controlling distortion of the noise.

    ```{admonition} Optional
    :class: tip

    This input is optional.

    If input is provided as a heightmap, the provided values are contextually mapped into directions, together with the `Distortion` parameter as the distance, this determines the distortion offset for sampling the noise.
    
    If the input is provided as a vectormap, the values from the vectormap are multiplied with the `Distortion` parameter and used contextually as the distortion offset for sampling the noise.
    ```

    ---

- {bdg-dark}`Mask` <label class="sd-text-secondary">Heightmap</label>

    Generator mask

    ```{admonition} Optional
    :class: tip

    This input is optional.
    ```

## Outputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Output` <label class="sd-text-secondary">Heightmap</label>

    Generated perlin heightmap

## Parameters
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Scale`

    The scale/period of the generated noise.

- {bdg-dark}`Amplitude`

    The amplitude of the generated noise.

- {bdg-dark}`Offset`

    The offset of the generated noise.

- {bdg-dark}`Octaves`

    The number of octaves to sample for the generated noise.

- {bdg-dark}`Persistence`

    The amplitude modification factor for each consecutive octave sampled.

- {bdg-dark}`Shape`

    - **STANDARD**

        Normal sampling with no transformation of the sampled values. Generates smooth shapes with elevations distributed on both sides of the offset as a midpoint.

    - **RIDGED**

        Sampled values are transformed using the inverted absolute value. Creates shapes imitating sharp connected mountain ridges and smooth isolated valleys. 

    - **BOWLY**

        Sampled values are transformed using the absolute value. Creates shapes imitating smooth isolated mountain hills and sharp connected valleys.

- {bdg-dark}`Seed`

    The seed used for generating the noise.

- {bdg-dark}`Distortion`

    The distortion strength used when sampling the noise. This requires an input provided to the `Distortion` input to have effect.