# Patcher
Patcher is the patch-editor (node-editor) part of the WorldSynth editor. When creating or opening a WorldSynth project, an instance of the Patcher editor is automatically opened of the main patch.

The editor let's you add, remove, move and edit the parameters of modules (nodes) in a patch.

![Patcher editor](images/patcher.png)


## Editor navigation
To move around in the node view, press and hold either `RMB` or `MMB` and drag to move the view.

To zoom in and out, use the `Scroll` wheel. This will zoom in or out centered on the mouse cursor.


## Adding modules
To add modules, `RMB` click anywhere in side the node editor area. This will open a context menu with several categories of modules for different datatypes or purposes, then navigate through the menu until selecting the desired module.

![Add module context menu](images/patcher_add_context.png)

When a module is selected, a preview of the module will attach to the mouse cursor which can be moved to the desired position.

To place the module, `LMB` click on the position you want to place it, and a new instance of the module will be added to the patch.

Now you can either continue with place further instances if you need more of the same module, or cancel further operations by pressing `Esc` key or clicking `RMB` with the mouse.


### Add with search
An alternative to using the context menu for adding modules is using the search addition. When working in the patch editor, press the `SPACE` key to open a search field, then start writing the name, or position in the context menu, of the desired module, using spaces to separate parts that should be combined in the search. The search will show a list modules matching the search, which you can select from at any time.

![Add module search](images/patcher_add_search_1.png)
![Add module search](images/patcher_add_search_2.png)


### Auto re-connect
While positioning the new module, before it is added to the patch with a `LMB` click, the module can be moved over top of a connector until the connector is highlighted. Then when adding the module with a `LMB` click, it will be spiced in between the connect modules, replacing the connector with two new connections with the new module in the line.

This automatic re-connections is added as a separate step to the edit history, so if the new connectors are undesired, performing one undo operation will remove these connections and restore the original connector again, and a second undo operation will remove the new module again entirely.


## Selecting modules
Modules are selected by `LMB` clicking on them. To add/remove multiple modules to the selection, use `Ctrl + LMB` clicking.

When selecting a module, the last selection will be used for the preview, unless the preview is locked.


### Box and lasso selection
It is also possible to perform box selection or lasso selection.

For box selection, press and hold `LMB` as you drag a box enclosing all the modules to select.

For lasso selection, hold down the `Ctrl` key when pressing `LMB` starting the selection, then drag the lasso to enclose all the modules to select.

The box and lasso selection will not change the preview selection.


## Moving modules
To move a module, move the mouse cursor over the module to be moved, the press and hold `LMB` as you drag the module to the new position. The move operation will end when releasing `LMB`.

When multiple modules are selected, moving one of the selected modules, will also move the rest of the selection.

If it is not desired to move all the module in the selection, hold down the `Ctrl` key when pressing `LMB` to move only one module of the selection.

When moving a module that is not part in the selection, it will only move that one module.


## Remove modules
To remove a module, select the module by `LMB` clicking on it to select it, then press the `Delete` key to remove it.

If using the `Delete` key when multiple modules are selected, it will delete all the selected modules.

Alternatively you can `RMB` click on the module to open the module context menu, and use the `Delete` option. This does not require the module to be selected, which avoids changing the preview node if the preview is not locked, of if you want to keep any other selections which may be active.

![Module context delete](images/patcher_module_context_delete.png)


### Auto re-connect
When a bypassable module is removed, any connections to the input and output will be bridged by a new connection in their place.

Any such automatic re-connections are added as a separate step to the edit history, so if the new connectors are undesired, performing one undo operation will remove these connections, and a second undo operation will restore the removed modules and connections to their original state before the removal.


## Editing modules
To edit a module, double `LMB` clicking on the module will open a parameters window for the module. This will also select the module and set it as the preview unless the preview is locked.

Alternatively you can `RMB` click on the module to open the module context menu, and use the `Parameters` option. This does not select the module or change the preview.

![Module context edit](images/patcher_module_context_edit.png)

If the module is selected, the keyboard shortcut `Crtl + E` will equally open the parameter editor.


## Bypass modules
To toggle bypass a module, you can `RMB` click on the module to open the module context menu, and use the `Bypass` option.

![Module context bypass](images/patcher_module_context_bypass.png)

If the module is selected, the keyboard shortcut `Crtl + B` will equally toggle bypass on the module.

Bypassing modules only works on a module if the module is bypassable, not all modules are bypassable.


## Rename modules
It's possible to give custom names to modules. This custom name will then show above the module, with the module type following in parenthesis.

To add or change a custom name for a module, you can `RMB` click on the module to open the module context menu, and use the `Rename` option (this does not require the module to be selected).

![Module context rename](images/patcher_module_context_rename.png)

This wil open a window for renaming the module.

![Module rename window](images/patcher_module_rename.png)

If the module is selected, the keyboard shortcut `Crtl + R` will equally open the name editor.


## Locking preview to a module
It is possible to lock the preview to a specific module, to do this, you can select a module and press the padlock button at the corner of the preview,

![Preview button lock preview](images/preview_module_lock.png)

 or you can `RMB` click on the module to open the module context menu, and use the `Lock preview` option.

![Module context lock preview](images/patcher_module_context_lockpreview.png)

A preview-locked module will be highlighted in the editor with a yellow glow, while a module with a preview that is not locked will have a white glow.

![Module with locked preview](images/patcher_module_preview_locked.png)
![Module with unlocked preview](images/patcher_module_preview_unlocked.png)