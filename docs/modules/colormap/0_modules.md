# Colormap modules

```{toctree}
:hidden:

```

## Generators
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Constant
:::

:::{grid-item-card} Colormap from materialmap
:::

:::{grid-item-card} Gradient
:::

:::{grid-item-card} HSL
:::

:::{grid-item-card} HSV
:::

:::{grid-item-card} RGB
:::

::::

## Modifiers
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Translate
:::

::::

## Combiners
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Combiner
:::

:::{grid-item-card} Selector
:::

::::

## IO
::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Colormap exporter
:::

:::{grid-item-card} Colormap importer
:::

::::