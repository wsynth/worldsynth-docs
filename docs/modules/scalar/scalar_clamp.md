# Scalar clamp

The clamp module limits the input value to be between the high and low clamp value such that:

`low clamp <= output <= high clamp`

## Inputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Input` <label class="sd-text-secondary">Scalar</label>

    Input value to be clamped

---

- {bdg-dark}`High` <label class="sd-text-secondary">Scalar</label>

    Higher clamp value

    ```{admonition} Optional
    :class: tip

    This input is optional. If input is provided, the provided value is used instead of the the `High clamp` parameter.
    ```

---

- {bdg-dark}`Low` <label class="sd-text-secondary">Scalar</label>

    Lower clamp value

    ```{admonition} Optional
    :class: tip

    This input is optional. If input is provided, the provided value is used instead of the the `Low clamp` parameter.
    ```

## Outputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Output` <label class="sd-text-secondary">Scalar</label>

    Clamped scalar value

## Parameters
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`High clamp`

    Higher clamp value

- {bdg-dark}`Low clamp`

    Lower clamp value
