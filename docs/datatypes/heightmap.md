# Heightmap
The heightmap is one of the most used datatypes, usually representing terrain elevation, but it can also be used for masks, or other single value properties in a two dimensional space.

Heigtmaps are usually seen as grayscale images representing the elevation of a terrain, where black is the lowest elevation, and white is the highest. While this is generally how elevations are represented as a grayscale image, WorldSynth internally stores elevations as absolute floating point values per coordinate, and can have generally any value that can be represented in a `flaot`, that's including negative values.

## Normalized elevation
For some uses, like when representing the elevation as a grayscale map, exporting heightmaps to images, used them for masking, and for some other use cases. It's advantageous or necessary to normalize an elevation range to be between 0-1.

For this, a normal normalized elevation is selected that proportionally maps to 1 (i.e. white), this elevation is 256 by default, meaning that values in the range 0-256 are mapped into the range 0-1, but this value can be change on a per project basis as a synth parameter.

The normalized elevation can be changed from the synth parameters editor, found in the menu `Edit > Synth parameters` as the parameter `Normalized Heightmap Height`.