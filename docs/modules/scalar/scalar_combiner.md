# Scalar combiner

The combiner module combines two input values using a selected operation.

## Inputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Primary` <label class="sd-text-secondary">Scalar</label>

    First input value

---

- {bdg-dark}`Secondary` <label class="sd-text-secondary">Scalar</label>

    Second input value

## Outputs
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Output` <label class="sd-text-secondary">Scalar</label>

    Combined value from the two inputs

## Parameters
<div class='sd-bg-secondary' style='width: 95%; height: 1px; margin: 0em 0em 0.1em 0em'></div>

- {bdg-dark}`Arithmetic operation`

    The operation applied to combine the two inputs values

    - `ADDITION`

        ```
            output = primary + secondary
        ```
    
    - `SUBTRACTION`

        ```
            output = primary - secondary
        ```

    - `MULTIPLICATION`

        ```
            output = primary * secondary
        ```
    
    - `DIVISION`

        ```
            output = primary / secondary
        ```

    - `MODULO`

        ```
            output = primary % secondary
        ```

        The modulo peration returns the remainder from a division of the primary input with the secondary input.

        For more information about the modulo operator, see the [modulo wikipedia article](https://en.wikipedia.org/wiki/Modulo_operation).

    - `AVERAGE`

        ```
            output = (primary + secondary) / 2
        ```
    
    - `MAX`

        ```
            output = max(primary, secondary)
        ```

    - `MIN`

        ```
            output = min(primary, secondary)
        ```