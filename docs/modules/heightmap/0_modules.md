# Heightmap modules

```{toctree}
:hidden:

constant
gradient
simple_perlin
fractal_perlin
```

## Generators
::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Cells
:img-bottom: images/cells.png
:::

:::{grid-item-card} Constant
:link: constant
:link-type: doc
:img-bottom: images/constant.png
:class-title: sd-text-primary
:::

:::{grid-item-card} Extent gradient
:img-bottom: images/extent_gradient.png
:::

:::{grid-item-card} Fractal perlin
:link: fractal_perlin
:link-type: doc
:class-title: sd-text-primary
:img-bottom: images/fractal_perlin.png
:::

:::{grid-item-card} Heightmap from blockspace
Generate heightmap from top block in a blockspace.
:::

:::{grid-item-card} Gradient
:link: gradient
:link-type: doc
:class-title: sd-text-primary
:img-bottom: images/gradient.png
:::

:::{grid-item-card} Simple perlin
:link: simple_perlin
:link-type: doc
:class-title: sd-text-primary
:img-bottom: images/simple_perlin.png
:::

:::{grid-item-card} Value noise
:img-bottom: images/value_noise.png
:::

:::{grid-item-card} Worley
:img-bottom: images/worley.png
:::

:::{grid-item-card} Worley conical
:img-bottom: images/worley_conical.png
:::

::::

## Modifiers
::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Affine transform
:::

:::{grid-item-card} Cell terrace
:img-bottom: images/cell_terrace.png
:::

:::{grid-item-card} Cellularize
:img-bottom: images/cellularize.png
:::

:::{grid-item-card} Clamp
:img-bottom: images/clamp.png
:::

:::{grid-item-card} Convexity
:img-bottom: images/convexity.png
:::

:::{grid-item-card} Distort
:img-bottom: images/distort.png
:::

:::{grid-item-card} Dither
:img-bottom: images/dither.png
:::

:::{grid-item-card} Expander
:img-bottom: images/expander.png
:::

:::{grid-item-card} Gain
:img-bottom: images/gain.png
:::

:::{grid-item-card} Inverter
:img-bottom: images/inverter.png
:::

:::{grid-item-card} MacSeralas pseudo erosion
:img-bottom: images/macseralas_pseudo_erosion.png
:::

:::{grid-item-card} Ramp
:img-bottom: images/ramp.png
:::

:::{grid-item-card} Resample
:img-bottom: images/resample.png
:::

:::{grid-item-card} Rescale
:img-bottom: images/rescale.png
:::

:::{grid-item-card} Slope
:img-bottom: images/slope.png
:::

:::{grid-item-card} Smoothen
:img-bottom: images/smoothen.png
:::

:::{grid-item-card} Terrace
:img-bottom: images/terrace.png
:::

:::{grid-item-card} Translate
:img-bottom: images/translate.png
:::

::::

## Math modifiers
::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Absolute
:::

:::{grid-item-card} Exponentiation
:::

:::{grid-item-card} Signum
:::

:::{grid-item-card} Trigonometry
:::

::::

## Selectors
Selectors are used to create masks from properties of a heightmap.

::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Convexity selector
:::

:::{grid-item-card} Height selector
:::

:::{grid-item-card} Slope selector
:::



::::

## Combiners
Functions for blending multiple heightmaps together.

::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Combiner
:::

:::{grid-item-card} Selector
Select among multiple heightmaps according to biomes.
:::

:::{grid-item-card} Smooth min/max
:::

::::

## IO
::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Heightmap exporter
:::

:::{grid-item-card} Heightmap importer
:::

::::

## Others
::::{grid} 1 2 2 3
:gutter: 4

:::{grid-item-card} Heightmap scatter
:::

::::
