# Scalar modules

```{toctree}
:hidden:

scalar_constant
scalar_clamp
scalar_combiner
```

## Generators
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Scalar constant
:link: scalar_constant
:link-type: doc
:::
::::

## Modifiers
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Scalar clamp
:link: scalar_clamp
:link-type: doc
:::
::::

## Combiners
::::{grid} 1 2 2 3
:gutter: 2

:::{grid-item-card} Scalar combiner
:link: scalar_combiner
:link-type: doc
:::
::::
