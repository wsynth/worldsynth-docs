# Addons
WorldSynth supports the usage of addons, capable of adding new functionality.

Addons can be added to WorldSynth by placing the addon's `.jar` file in the addon directory found in the WorldSynth root directory before starting WorldSynth.

An addon will usually add new modules, but a more can also add other functionality like:
- Modules
- [Datatypes](datatypes/0_datatypes.md)
- Object formats
- [Material profiles](resources/materialsformat.md)
- Biome profiles