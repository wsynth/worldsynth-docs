# Biomes file format

WorldSynth allows for defining biomes using biomes files in the JSON format. This aims at being an easy but flexible way of creating biome definitions that can be reused across projects and easily shared.

Biomes files are stored in the biomes directory found in the WorldSynth root directory, and are arbitrarily named .json files with content adhering to the biomes JSON format.

 ```{admonition} Minecraft biomes
:class: note

For Minecraft biomes, additional helpful resources for verification of the JSON content, and examples, can be found in the [WorldSynth biomes Minecraft](https://gitlab.com/wsynth/worldsynth-materials-minecraft) repository.
```

## A simple biome example

```json
{
    "default": {
        "generic:desert": {
            "displayName": "Generic Desert"
        }
    }
}
```

This is a simple example of the content of a biomes file that just adds a generic desert biome with a custom display name of "Generic Desert" that will be used in the editor. This is imported according to the `default` biome profile.
This JSON data is stored in a file with an arbitrary name and ".json" as suffix, and placed inside the biomes directory. When WorldSynth is started it'll parse biomes from any JSON file in the biomes directory or sub-directory contained inside the biomes directory.

## Biome profiles

Biome profiles in WorldSynth define what information a biome can store. The base profile in WorldSynth is the `default` profile, but addons can define new profiles that extend on the `default` profile to store additional information that the addon can use, an example of such an extended profile is the `minecraft` biome profile created by the Minecraft addon.

## Default profile

```json
{
    "default": {
        "idName[ex: desert]": {
            "displayName": "Desert",
            "color": [123, 123, 123]
        }
    }
}
```

### Minimal example

As shown, the default biome profile defines several fields of information that WorldSynth uses. In it's easiest form, a biome defined with the default profile doesn't require any of these fields to actually be presents beyond the biome root, so the simplest valid definition for adding our generic stone biome would be:

```json
{
    "default": {
        "desert": {}
    }
}
```

### Biome fields overview


* `idName`

    The id name of the biome

    ```{admonition} Warning
    :class: warning

    The idName is used for storing biomes in a synth. If the idName changes, a biome will not be recognized from old projects.
    ```

* `displayName`

    **default**: Nothing

    The display name is a custom name that may be displayed in the editor UI.
    
    If no display name is provided, the default will be to display the `idName` in the editor UI.

* `color`

    **default**: `[255, 0, 255]` Magenta color

    The representative color of the biome represented as an array of RGB values in 0-255 integer format `"color": [r, g, b]`.