# Materials file format

WorldSynth allows for defining materials using materials files in the JSON format. This aims at being an easy but flexible way of creating material definitions that can be reused across projects and easily shared.

Materials files are stored in the materials directory found in the WorldSynth root directory, and are arbitrarily named .json files with content adhering to the materials JSON format.

 ```{admonition} Minecraft materials
:class: note

For Minecraft materials, additional helpful resources for verification of the JSON content, and examples, can be found in the [WorldSynth Materials Minecraft](https://gitlab.com/wsynth/worldsynth-materials-minecraft) repository.
```

## A simple material example

```json
{
    "default": {
        "generic:stone": {
            "displayName": "Generic Stone"
        }
    }
}
```

This is a simple example of the content of a materials file that just adds a generic stone material with a custom display name of "Generic Stone" that will be used in the editor. This is imported according to the `default` material profile.
This JSON data is stored in a file with an arbitrary name and ".json" as suffix, and placed inside the materials directory. When WorldSynth is started it'll parse materials from any JSON file in the materials directory or sub-directory contained inside the materials directory.

## Material profiles

Material profiles in WorldSynth define what information a material can store. The base profile in WorldSynth is the `default` profile, but addons can define new profiles that extend on the `default` profile to store additional information that the addon can use, an example of such an extended profile is the `minecraft` material profile created by the Minecraft addon.

## Default profile

```json
{
    "default": {
        "idName[ex: stone]": {
            "displayName": "Stone",
            "color": [123, 123, 123],
            "texture": "textures/stone_texture.png",
            "isAir": false,
            "properties": {
                "coloring": ["light", "dark"],
                "property2": ["p2_value1", "p2_value2"]
            },
            "states": [
                {
                    "properties": {
                        "coloring": "light",
                        "property2": "p2_value1"
                    },
                    "default": true,
                    "displayName": "Light stone"
                },
                {
                    "properties": {
                        "coloring": "dark",
                        "property2": "p2_value1"
                    },
                    "displayName": "Dark stone",
                    "color": [234, 234, 234],
                    "texture": "textures/dark_stone_texture.png"
                }
            ]
        }
    }
}
```

### Minimal example

As shown, the default material profile defines several fields of information that WorldSynth uses. In it's easiest form, a material defined with the default profile doesn't require any of these fields to actually be presents beyond the material root, so the simplest valid definition for adding our generic stone material would be:

```json
{
    "default": {
        "stone": {}
    }
}
```

### Material fields overview


* `idName`

    The id name of the material

    ```{admonition} Warning
    :class: warning

    The idName is used for storing materials in a synth. If the idName changes, a material will not be recognized from old projects.
    ```

* `displayName`

    **default**: Nothing

    The display name is a custom name that may be displayed in the editor UI.
    
    If no display name is provided, the default will be to display the `idName` in the editor UI.

* `color`

    **default**: `[255, 0, 255]` Magenta color

    The representative color of the material represented as an array of RGB values in 0-255 integer format `"color": [r, g, b]`.

* `texture`

    **default**: Nothing

    The file path to a texture image relative to the materials file.

* `isAir`

    **default**: `false`

    A boolean value `true` or `false`, identifying weather this material is considered to be air.

    Default value is `false` if not defined.

* `properties`

    **default**: Nothing

    A map of one ore more properties with their possible values.

    ```{admonition} Warning
    :class: warning

    The properties is used for storing material states in a synth. If the properties of a material changes, states of the material will not be recognized from old projects.
    ```

* `states`

    **default**: Nothing

    An array of states for this material. This doesn't need to be exhaustive, states not defined will be extrapolated and inherit all values from the material.


### Material states fields overview

* `properties`

    The values of the properties associated with the state.

* `default`

    **default**: `false`

    The default field is used to define a state as the default state of the material, there can only be one default state.

* `displayName`

    **default**: Nothing

    A custom display name for this state.

* `color`

    **default**: Nothing

    A custom color for this state.

* `texture`

    **default**: Nothing

    A custom texture for this state.